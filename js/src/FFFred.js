var App = App || {};
App.FFFred = (function () {
    "use strict";
    /* eslint-env browser */
    /*global PIXI */

    function createFred(fredType) {
        var texture1,
            texture2,
            textures,
            fred;

        switch (fredType) {
        case 1:
            texture1 = PIXI.Texture.fromImage('res/images/rfastFred1.png');
            texture2 = PIXI.Texture.fromImage('res/images/rfastFred2.png');
            break;
        case 2:
            texture1 = PIXI.Texture.fromImage('res/images/rfred1.png');
            texture2 = PIXI.Texture.fromImage('res/images/rfred2.png');
            break;
        case 3:
            texture1 = PIXI.Texture.fromImage('res/images/rstrongFred1.png');
            texture2 = PIXI.Texture.fromImage('res/images/rstrongFred2.png');
            break;
        default:
            break;
        }

        textures = [texture1, texture2];
        fred = new PIXI.extras.MovieClip(textures);
        fred.width /= 2;
        fred.height /= 2;
        fred.x = 1;
        fred.y = 1;
        fred.vx = 0;
        fred.vy = 0;
        fred.animationSpeed = 0.05;
        return fred;


    }

    function updateFred(turned, fishID) {
        var texture1,
            texture2,
            textures;

        if (turned) {
            switch (fishID) {
            case 1:
                texture1 = PIXI.Texture.fromImage('res/images/rfastFred1.png');
                texture2 = PIXI.Texture.fromImage('res/images/rfastFred2.png');
                break;
            case 2:
                texture1 = PIXI.Texture.fromImage('res/images/rfred1.png');
                texture2 = PIXI.Texture.fromImage('res/images/rfred2.png');
                break;
            case 3:
                texture1 = PIXI.Texture.fromImage('res/images/rstrongFred1.png');
                texture2 = PIXI.Texture.fromImage('res/images/rstrongFred2.png');
                break;
            default:
                break;
            }

        } else {
            switch (fishID) {
            case 1:
                texture1 = PIXI.Texture.fromImage('res/images/fastFred1.png');
                texture2 = PIXI.Texture.fromImage('res/images/fastFred2.png');
                break;
            case 2:
                texture1 = PIXI.Texture.fromImage('res/images/fred1.png');
                texture2 = PIXI.Texture.fromImage('res/images/fred2.png');
                break;
            case 3:
                texture1 = PIXI.Texture.fromImage('res/images/strongFred1.png');
                texture2 = PIXI.Texture.fromImage('res/images/strongFred2.png');
                break;
            default:
                break;
            }
        }
        textures = [texture1, texture2];
        return textures;
    }

    function invFred(turned) {
        var texture1,
            texture2,
            textures;

        if (turned) {
            texture1 = PIXI.Texture.fromImage('res/images/rinvFred1.png');
            texture2 = PIXI.Texture.fromImage('res/images/rinvFred2.png');
        } else {
            texture1 = PIXI.Texture.fromImage('res/images/invFred1.png');
            texture2 = PIXI.Texture.fromImage('res/images/invFred2.png');
        }
        textures = [texture1, texture2];
        return textures;
    }

    return {
        createFred: createFred,
        updateFred: updateFred,
        invFred: invFred
    };





}());