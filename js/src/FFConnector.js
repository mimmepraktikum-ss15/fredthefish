var App = App || {};
App.FFConnector = (function () {
    "use strict";
    /* eslint-env browser */
    /*jslint plusplus: true */
    /*global $, Parse, alert, console */

    var LOCAL_KEY = "FredScore",
        QUERY;

    function initParse() {
        Parse.initialize("ajSfNyxWggYiLmoVoNm8wHxKn2ScwwdtSPBefD9L", "obWBD2ybZ83MCABLpBjw8Ddpw1rBuA1fyEmR4cpT");
    }

    /*jslint nomen: true*/
    function getList(_callback) {
        /*jslint nomen: false*/
        var query,
            GameScore,
            ScoreList = [];
        GameScore = Parse.Object.extend("GameScore");
        query = new Parse.Query(GameScore);
        query.find({
            success: function (results) {
                var i,
                    tempObj;
                for (i = 0; i < results.length; i++) {
                    tempObj = {};
                    tempObj.Name = results[i].get("Name");
                    tempObj.Score = results[i].get("Score");
                    tempObj.ID = results[i].id;
                    ScoreList.push(tempObj);
                }
                ScoreList.sort(function (a, b) {
                    return parseFloat(a.Score) - parseFloat(b.Score);
                });
                QUERY = ScoreList;
                /*jslint nomen: true*/
                _callback();
                /*jslint nomen: false*/

            },
            error: function (error) {
                alert("Error: " + error.code + " " + error.message);
            }
        });

    }


    function getQuery() {
        return QUERY;
    }
    
    
    function enterScore(final) {
        var query,
            GameScore,
            thisScore;
        GameScore = Parse.Object.extend("GameScore");
        thisScore = new GameScore();
        thisScore.set("Score", final.Score);
        thisScore.set("Name", final.Name);
        thisScore.save(null, {
            success: function (gameScore) {


                App.FFStarter.updateGlobal();
            },
            error: function (gameScore, error) {
                // Execute any logic that should take place if the save fails.
                // error is a Parse.Error with an error code and message.
                alert('Failed to create new object, with error code: ' + error.message);
            }
        });

    }


    function deleteScore(int) {}

 

    function compareScore(List, final) {
        var entered = false, i;
        if (List.length === 20) {
            
            for (i = 0; i < List.length; i++) {
                if (final.Score > List[i].Score && entered === false) {
                    List.splice(i, 0, final);
                    entered = true;
                }
            }
            if (List.length > 20) {
                List.slice(0, 20);
                enterScore(final);
                deleteScore(List[20].ID);
            }
        }
        if (List.length < 20) {
            List.push(final);
            enterScore(final);
        }
        return List;
    }
    
    function pushList(List, final) {
        var updatedList = compareScore(List, final);
        QUERY = updatedList;
    }




    function pushScore(final) {
        getList(function () {
            pushList(getQuery(), final);
        });
    }
    
    
    function sortArray(thisArray) {
        var tempArray = [];
        if (thisArray !== undefined) {
            thisArray.sort(function (a, b) {
                return parseFloat(a.Score) - parseFloat(b.Score);
            });

            return thisArray;
        }
    }



    


    function getLocalList(callback) {
        var localScore;
        if (localStorage.getItem(LOCAL_KEY) !== null && localStorage.getItem(LOCAL_KEY) !== undefined) {
            localScore = JSON.parse(localStorage.getItem(LOCAL_KEY)) || {};
        } else {
            console.log("no entry in LocalStorage");
        }
        localScore = sortArray(localScore);
        return localScore;
    }
    
    
    
   

    function saveToLocalStorage(final) {
        var temp = [];
        temp.push(final);
        if (getLocalList() !== undefined) {
            temp.push.apply(temp, getLocalList());
        }

        localStorage.setItem(LOCAL_KEY, JSON.stringify(temp));

    }

  


    // Scores given as Object with Name and Score properties
    function compareHighscore(Final) {
        var List,
            newHighScore = false,
            i;
        List = getList();
        
        for (i = 0; i < List.length; i++) {
            if (List[i].Score < Final.score) {
                List.splice(i, 0, Final);
                newHighScore = true;
                List.pop();
            }
        }
        if (newHighScore === true) {
            pushList(List);
        }

        return List;
    }


    return {
        compare: compareHighscore,
        getLocal: getLocalList,
        save: saveToLocalStorage,
        getGlobal: getList,
        initParse: initParse,
        getQuery: getQuery,
        pushScore: pushScore

    };





}());