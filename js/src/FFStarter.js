var App = App || {};
App.FFStarter = (function () {
    "use strict";
    /* eslint-env browser */
    /*global $, alert */
    /*jslint plusplus: true */

    var startButton,
        nameInput,
        buttonList,
        submitButton,
        canvasContainer,
        title,
        headerElements,
        gameEngine,
        highScoreButton,
        swapListButton,
        returnFromListButton,
        scoreConnector,
        scoreList,
        scoreGroup,
        timeView,
        scoreView,
        countdownView,
        lifeView,
        score = 0,
        seconds = 0,
        count = 15,
        lifes = 0,
        playing = false,
        countdown = false,
        bonus = 1,
        fastFred,
        normalFred,
        strongFred,
        fredButtonList,
        highscoreMenu,
        fredTitle,
        global = false,
        afterGame = false,
        selectedFish,
        userName,
        nameEntry,
        alreadyEntered = true,
        listHeader,
        helpButton,
        helpReturnButton,
        helpScreen,
        cheatButton,
        cheatReturnButton,
        cheatScreen,
        cheatEnterButton,
        cheatEnterInput,
        cheatsActive,
        puTextView;
    
    
    function fillHighscoreList(list) {
        var listView = document.getElementById("highscore-list"),
            i,
            item;

        while (listView.hasChildNodes()) {
            listView.removeChild(listView.lastChild);
        }
        document.getElementById("highscore-list");
        if (list !== null && list !== undefined) {
            for (i = 0; i < list.length; i++) {
                item = document.createElement('li');
                item.appendChild(document.createTextNode(list[list.length - i - 1].Name + " : " + list[list.length - i - 1].Score));

                document.getElementById("highscore-list").appendChild(item);
            }
        }
    }

    
    function fillWithLocalScore() {

        var list = scoreConnector.getLocal();
        fillHighscoreList(list);

    }
    
    function fillWithNewLocalScore() {
        var playerObject = {};
        playerObject.Name = userName;
        playerObject.Score = score;
        scoreConnector.save(playerObject);
        fillWithLocalScore();
    }


    function fillWithNewGlobalScore() {
        var playerObject = {};
        playerObject.Name = userName;
        playerObject.Score = score;
        scoreConnector.pushScore(playerObject);
    }
    
    function fillGlobalSecondStep() {
        var list = scoreConnector.getQuery();
        fillHighscoreList(list);
    }

    function fillWithGlobalScore() {
        scoreConnector.getGlobal(function () {
            fillGlobalSecondStep();
        });


    }

   

   


      

   

    function onhighScoreClicked(event) {
        buttonList.classList.add("invisible");
        buttonList.classList.remove("visible");
        highscoreMenu.classList.remove("invisible");
        highscoreMenu.classList.add("visible");
        fredTitle.classList.add("smaller");
        fillWithLocalScore();



    }

    function returnFromHelp(event) {
        buttonList.classList.add("visible");
        buttonList.classList.remove("invisible");
        helpScreen.classList.remove("visible");
        helpScreen.classList.add("invisible");
    }


    function onHelpClicked(event) {
        buttonList.classList.add("invisible");
        buttonList.classList.remove("visible");
        helpScreen.classList.remove("invisible");
        helpScreen.classList.add("visible");
    }


    function returnFromCheat(event) {
        cheatScreen.classList.remove("visible");
        cheatScreen.classList.add("invisible");
        helpScreen.classList.remove("invisible");
        helpScreen.classList.add("visible");
    }


    function onCheatClicked(event) {
        helpScreen.classList.remove("visible");
        helpScreen.classList.add("invisible");
        cheatScreen.classList.remove("invisible");
        cheatScreen.classList.add("visible");
    }

    function onCheatEntered(event) {
        var cheatCode;
        cheatCode = cheatEnterInput.value;
        if (cheatCode === "FREDISAWESOME") {
            cheatsActive = true;
            alert("Cheats active, Global Highscore disabled");
        } else {
            alert("Wrong Cheat Code!");
        }
    }

    function setPUText(text) {


        puTextView.innerHTML = text;
        puTextView.classList.add("visible");
        puTextView.classList.remove("invisible");

        setTimeout(function () {
            puTextView.classList.add("invisible");
            puTextView.classList.remove("visible");
        }, 3000);
    }

    function updateScore(points) {

        score += points;
        scoreView.innerHTML = score;

    }


    function changeButton(string) {
        swapListButton.innerHTML = string;
    }

    function changeListName(string) {
        listHeader.innerHTML = string + " Highscore";
    }


    function onSwapClicked(event) {
        if (afterGame === false) {
            if (global === false) {

                changeButton("Local");
                changeListName("Global");
                fillWithGlobalScore();
                global = true;
            } else {
                changeButton("Global");
                changeListName("Local");
                fillWithLocalScore();
                global = false;
            }
        } else {
            if (global === false && !cheatsActive) {
                if (alreadyEntered === false) {
                    fillWithNewGlobalScore();
                    alreadyEntered = true;
                } else {
                    fillWithGlobalScore();
                }
                global = true;
            } else {
                alert("Cheat active, no Global Highscore possible");

                fillWithLocalScore();
                global = false;
            }

        }
    }

    function updateGlobal() {
        fillWithGlobalScore();
    }

    

    function onReturnClicked(event) {
        if (afterGame === true) {
            buttonList.classList.add("visible");
            buttonList.classList.remove("invisible");
            highscoreMenu.classList.remove("visible");
            highscoreMenu.classList.add("invisible");
            while (canvasContainer.hasChildNodes()) {
                canvasContainer.removeChild(canvasContainer.lastChild);
            }
            //gameEngine.destroy();
            afterGame = false;
        } else {
            buttonList.classList.add("visible");
            buttonList.classList.remove("invisible");
            highscoreMenu.classList.remove("visible");
            highscoreMenu.classList.add("invisible");
            afterGame = false;
        }

    }


    function setBonus(scoreBonus) {
        bonus += scoreBonus;

    }



    function onStartClicked(event) {
        nameInput.classList.remove("invisible");
        nameInput.classList.add("visible");
        buttonList.classList.remove("visible");
        buttonList.classList.add("invisible");
        fredButtonList.classList.remove("invisible");
        fredButtonList.classList.add("visible");
    }
    
    function getName() {
        userName = nameEntry.value;
        if (userName === undefined / userName === null) {
            userName = "Anonymus";
        }
        if (userName === "") {
            userName = "Anonymus";
        }
    }


    function onSubmitClicked(event) {
        selectedFish = this.id;
        score = 0;
        seconds = 0;
        bonus = 1;
        playing = true;
        canvasContainer.classList.remove("invisible");
        canvasContainer.classList.add("visible");
        nameInput.classList.remove("visible");
        nameInput.classList.add("invisible");
        //ADDED
        title.classList.remove("visible");
        title.classList.add("invisible");
        headerElements.classList.remove("invisible");
        headerElements.classList.add("visible");
        fredButtonList.classList.remove("visible");
        fredButtonList.classList.add("invisible");
        scoreView.innerHTML = score;
        timeView.innerHTML = seconds;
        gameEngine = new App.FFGameEngine();
        getName();
        alreadyEntered = false;

    }

   

    function setTime() {

        if (playing) {
            seconds++;
            timeView.innerHTML = seconds;

            if (seconds % 5 === 0 && seconds !== 0) {
                score += 10 * bonus;
                scoreView.innerHTML = score;
            }
        }
    }

    function getTime() {
        return seconds;
    }

    function setCountdown() {

        if (countdown) {
            if (count > 1) {
                count--;
                countdownView.innerHTML = "Invincible: " + count;
            } else {
                countdown = false;
                count = 15;
                countdownView.classList.add("invisible");
                countdownView.classList.remove("visible");

            }
        }
    }

    function setLifes(hitpoints) {

        lifes = hitpoints;
        lifeView.innerHTML = hitpoints + " x";
    }

    function getFish() {
        return selectedFish;
    }

    function gameOver() {
        highscoreMenu.classList.remove("invisible");
        highscoreMenu.classList.add("visible");
        canvasContainer.classList.remove("visible");
        canvasContainer.classList.add("invisible");
        headerElements.classList.remove("visible");
        headerElements.classList.add("invisible");
        title.classList.remove("invisible");
        title.classList.add("visible");
        fredTitle.classList.add("smaller");
        afterGame = true;
        fillWithNewLocalScore();
    }


    function getScore() {
        return score.toString();
    }


   

    function changePlaying() {
        playing = false;
    }

    function startCountdown(active) {
        countdown = active;

        countdownView.classList.add("visible");
        countdownView.classList.remove("invisible");

    }

    function checkCheatsActive() {
        return cheatsActive;

    }
    
    function setupUI() {

        timeView = document.getElementById("time");
        scoreView = document.getElementById("score");
        lifeView = document.getElementById("lifes");

        countdownView = document.getElementById("count");
        countdownView.classList.add("invisible");
        countdownView.classList.remove("visible");

        puTextView = document.getElementById("puText");


        headerElements = document.getElementById("header");
        title = document.getElementById("title");
        canvasContainer = document.getElementById("canvas-cover");

        nameInput = document.getElementById("Playername");
        buttonList = document.getElementById("main-menu");
        startButton = document.getElementById("gameButton");
        startButton.addEventListener("click", onStartClicked, false);

        highScoreButton = document.getElementById("highScoreButton");
        highScoreButton.addEventListener("click", onhighScoreClicked, false);

        helpButton = document.getElementById("helpButton");
        helpButton.addEventListener("click", onHelpClicked, false);

        helpReturnButton = document.getElementById("returnFromHelpButton");
        helpReturnButton.addEventListener("click", returnFromHelp, false);

        helpScreen = document.getElementById("helpPage");

        cheatButton = document.getElementById("cheatButton");
        cheatButton.addEventListener("click", onCheatClicked, false);

        cheatReturnButton = document.getElementById("returnFromCheatButton");
        cheatReturnButton.addEventListener("click", returnFromCheat, false);

        cheatScreen = document.getElementById("cheatPage");

        cheatEnterInput = document.getElementById("cheatCode");
        cheatEnterButton = document.getElementById("cheatEnterButton");
        cheatEnterButton.addEventListener("click", onCheatEntered, false);


        nameEntry = document.getElementById("nameInput");

        swapListButton = document.getElementById("swapListButton");
        returnFromListButton = document.getElementById("returnFromListButton");

        swapListButton.addEventListener("click", onSwapClicked, false);
        returnFromListButton.addEventListener("click", onReturnClicked, false);

        highscoreMenu = document.getElementById("highscore-menu");

        scoreList = document.getElementById("scoreList");
        scoreGroup = document.getElementById("scoreGroup");

        fastFred = document.getElementById("fastFred");
        normalFred = document.getElementById("normalFred");
        strongFred = document.getElementById("strongFred");

        fastFred.addEventListener("click", onSubmitClicked, false);

        normalFred.addEventListener("click", onSubmitClicked, false);

        strongFred.addEventListener("click", onSubmitClicked, false);

        fredButtonList = document.getElementById("fishSelect");

        fredTitle = document.getElementById("title");

        listHeader = document.getElementById("highscoreHeader");
    }
    
    function init() {
        setupUI();
        scoreConnector = App.FFConnector;
        setInterval(setTime, 1000);
        setInterval(setCountdown, 1000);
        scoreConnector.initParse();

    }

    return {
        init: init,
        updateScore: updateScore,
        getFish: getFish,
        gameOver: gameOver,
        changePlaying: changePlaying,
        startCountdown: startCountdown,
        getScore: getScore,
        updateGlobal: updateGlobal,
        setBonus: setBonus,
        checkCheatsActive: checkCheatsActive,
        setLifes: setLifes,
        getTime: getTime,
        setPUText: setPUText

    };


}());