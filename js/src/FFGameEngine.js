var App = App || {};
App.FFGameEngine = function () {
    "use strict";
    /* eslint-env browser */
    /*global $, PIXI, Howl, keyboard, requestAnimationFrame */
    /*jslint plusplus: true */


    var collision,
        objects,
        starter,
        fredApp,
        stage,
        gameScene,
        gameOverScene,
        renderer,
        fred,
        bg,
        shark,
        powerup,
        hurtEffect,
        fredHeight = 0,
        fredWidth = 0,
        position = 0,
        fishID,
        hooks = [],
        numberOfHooks = 5,
        worms = [],
        numberOfWorms = 6,
        playing = true,
        scoreMessage,
        hitpoints = 0,
        active = true,
        turned = false,
        invincible = false,
        GENERAL_HEIGHT = 500,
        GENERAL_WIDTH = 1200,
        GENERAL_OBJECT_HEIGHT = 400,
        GENERAL_OBJECT_WIDTH = 1100,
        powerupApp,
        puFunction,
        puScoreBonus = 1,
        cheatBonus = 1,
        powerupActive = false;



    function checkSharkHitsWall() {

        var sharkHitsWall = collision.contain(shark, {
                x: 0,
                y: 0,
                width: GENERAL_WIDTH,
                height: GENERAL_HEIGHT
            }),
            textures,
            textureShark1,
            textureShark2;


        // NEW TEXTURE NEW CODE
        if (sharkHitsWall === "left") {
            active = true;
            textureShark1 = PIXI.Texture.fromImage('res/images/shark1.png');
            textureShark2 = PIXI.Texture.fromImage('res/images/shark2.png');
            textures = [textureShark1, textureShark2];
            shark.textures = textures;
            shark.vx *= -1;
        } else if (sharkHitsWall === "right") {
            active = true;
            textureShark1 = PIXI.Texture.fromImage('res/images/lshark1.png');
            textureShark2 = PIXI.Texture.fromImage('res/images/lshark2.png');
            textures = [textureShark1, textureShark2];

            shark.textures = textures;
            shark.vx *= -1;
        } else if (sharkHitsWall === "top" || sharkHitsWall === "bottom") {
            active = true;

            shark.vy *= -1;
        }
    }


    function moveShark() {
        shark.y += shark.vy;
        shark.x += shark.vx;


        checkSharkHitsWall();

    }


    function displayHurtEffect() {
        hurtEffect.visible = true;
        setTimeout(function () {
            hurtEffect.visible = false;
        }, 250);
    }

    function growing() {

        if (fred.width < 110 && fred.height < 70) {
            fred.scale.x *= 1.05;
            fred.scale.y *= 1.05;
        }
    }

    function invincibleFred() {
        var textures,
            actFredHeight,
            actFredWidth;

        actFredHeight = fred.height;
        actFredWidth = fred.width;
        textures = fredApp.invFred(turned);
        fred.textures = textures;
        fred.height = actFredHeight;
        fred.width = actFredWidth;
    }

    function changeFred() {
        var textures,
            actFredHeight,
            actFredWidth;

        actFredHeight = fred.height;
        actFredWidth = fred.width;
        textures = fredApp.updateFred(turned, fishID);
        fred.textures = textures;
        fred.height = actFredHeight;
        fred.width = actFredWidth;

    }


    function powerupFunction() {
        switch (puFunction) {
        case 1:
            starter.setPUText("More Points");
            puScoreBonus *= 2;
            starter.setBonus(puScoreBonus);
            break;
        case 2:
            starter.setPUText("500 Bonus Points");
            starter.updateScore(500);
            break;
        case 3:
            starter.setPUText("Smallest State");
            fred.width = fredWidth;
            fred.height = fredHeight;

            break;
        case 4:
            if (!invincible) {
                invincibleFred();
                starter.startCountdown(true);
                invincible = true;
                setTimeout(function () {
                    invincible = false;
                    changeFred();
                }, 16000);
            }
            break;
        case 5:
            starter.setPUText("-300 Points");
            starter.updateScore(-300);

            break;

        case 6:
            starter.setPUText("+1 Life");
            hitpoints++;
            starter.setLifes(hitpoints);

            break;
        default:
            break;
        }

    }


    function checkFredHitsWall() {

        var fredHitsWall = collision.contain(fred, {
            x: 0,
            y: 0,
            width: GENERAL_WIDTH,
            height: GENERAL_HEIGHT
        });


        if (fredHitsWall === "left") {
            fred.x = 0;
        } else if (fredHitsWall === "right") {
            fred.x = GENERAL_WIDTH - fred.width;
        } else if (fredHitsWall === "top") {
            fred.y = 0;
        } else if (fredHitsWall === "bottom") {
            fred.y = GENERAL_HEIGHT - fred.height;
        }
    }

    function sharkPresent() {
        if (shark !== undefined) {
            return true;
        } else {
            return false;
        }
    }

    function newShark(sharkType) {

        shark = objects.newShark(GENERAL_WIDTH, GENERAL_HEIGHT, sharkType);
        gameScene.addChild(shark);
        shark.play();
    }

    function newWorm() {
        var worm;
        worm = objects.newWorm(GENERAL_OBJECT_WIDTH, GENERAL_OBJECT_HEIGHT);
        worms.push(worm);

        gameScene.addChild(worm);

    }

    function newPowerup() {
        powerup = powerupApp.newPowerup(GENERAL_OBJECT_WIDTH, GENERAL_OBJECT_HEIGHT);

        gameScene.addChild(powerup);

    }

    function newHook() {
        var hook;
        hook = objects.newHook(GENERAL_OBJECT_WIDTH, GENERAL_OBJECT_HEIGHT);
        hooks.push(hook);

        gameScene.addChild(hook);
    }

    function gameEnd() {
        var message, sound;
        sound = new Howl({
            urls: ['../res/sounds/gameover.mp3']
        }).play();
        playing = false;
        starter.changePlaying();
        message = "Game Over " + "\n" + starter.getScore() + " Points";
        scoreMessage = new PIXI.Text(
            message,
            {
                font: "64px Futura",
                fill: "white"
            }
        );
        scoreMessage.x = 450;
        scoreMessage.y = 150;
        gameOverScene.addChild(scoreMessage);
        gameScene.visible = false;
        gameOverScene.visible = true;
        setTimeout(function () {
            starter.gameOver();

        }, 1500);

    }

    function shrinking() {

        if (fred.width >= fredWidth && fred.height >= fredHeight) {
            fred.scale.x *= 0.8;
            fred.scale.y *= 0.8;
        }

        fred.x += 40;

    }


    function checkHitShark() {

        if (collision.hitTestRectangle(fred, shark) && !invincible) {


            if (hitpoints < 1) {
                gameEnd();
            } else if (active === true) {
                var sound = new Howl({
                    urls: ['../res/sounds/hit.wav']
                }).play();
                active = false;
                displayHurtEffect();
                shrinking();
                hitpoints--;
                starter.setLifes(hitpoints);

            }
            setTimeout(function () {}, 1000);
        }
    }



    function checkShark() {
        if (starter.getScore() > 1000 && starter.getScore() < 1800 && sharkPresent() === false) {
            newShark(1);
            starter.setBonus(10);
        }
        if (starter.getScore() > 1800 && starter.getScore() < 3000 && sharkPresent() === true) {
            gameScene.removeChild(shark);
            shark = undefined;
            starter.setBonus(1);

        }

        if (starter.getScore() > 3000 && starter.getScore() < 4000 && sharkPresent() === false) {
            newShark(1);
            starter.setBonus(10);
        }

        if (starter.getScore() > 4000 && starter.getScore() < 6000 && sharkPresent() === true) {
            gameScene.removeChild(shark);
            shark = undefined;
            starter.setBonus(1);

        }

        if (starter.getScore() > 6000 && starter.getScore() < 8000 && sharkPresent() === false) {
            newShark(1);
            starter.setBonus(10);
        }

        if (starter.getScore() > 8000 && starter.getScore() < 12000 && sharkPresent() === true) {
            gameScene.removeChild(shark);
            shark = undefined;
            starter.setBonus(1);

        }

        if (starter.getScore() > 12000 && sharkPresent() === false) {
            newShark(2);
            starter.setBonus(100);
        }



        if (sharkPresent()) {
            moveShark();
            moveShark();
            checkHitShark();
        }
    }







    function checkHitHook() {

        var i, sound;
        for (i = 0; i < hooks.length; i++) {
            if (collision.hitTestRectangle(fred, hooks[i])) {
                if (fred.height < fredHeight && fred.width < fredWidth && !invincible) {
                    gameEnd();
                } else if (!invincible) {
                    sound = new Howl({
                        urls: ['../res/sounds/hook.wav']
                    }).play();
                    displayHurtEffect();
                    shrinking();
                }
                gameScene.removeChild(hooks[i]);
                hooks.splice($.inArray(hooks[i], hooks), 1);
                newHook();
            }
        }

    }




    function checkEating() {
        var i, sound;
        for (i = 0; i < worms.length; i++) {
            if (collision.hitTestRectangle(fred, worms[i])) {
                starter.updateScore(100 * puScoreBonus * cheatBonus);

                sound = new Howl({
                    urls: ['../res/sounds/pickup.wav']
                }).play();

                gameScene.removeChild(worms[i]);
                worms.splice($.inArray(worms[i], worms), 1);
                newWorm();
                growing();
            }
        }
    }


    function checkPowerup() {

        if (starter.getTime() % 10 === 0 && !powerupActive && !invincible) {
            powerupActive = true;
            newPowerup();

        }
        if (starter.getTime() % 10 === 3 && powerupActive) {
            powerupActive = false;
            gameScene.removeChild(powerup);
        }
        if (powerup !== null) {
            if (collision.hitTestRectangle(fred, powerup) && powerupActive) {
                var sound = new Howl({
                    urls: ['../res/sounds/powerup.wav']
                }).play();
                gameScene.removeChild(powerup);
                puFunction = powerupApp.choosePowerupFunction();
                powerupFunction();
                powerupActive = false;
            }
        }
    }





    function setupBackground() {
        bg = PIXI.Sprite.fromImage('res/images/underwater.png');
        bg.width = renderer.width;
        bg.height = renderer.height;
        gameScene.addChild(bg);
    }

    function setupFred() {

        switch (starter.getFish()) {
        case "fastFred":
            fishID = 1;
            hitpoints = 1;
            break;
        case "normalFred":
            fishID = 2;
            hitpoints = 3;
            break;
        case "strongFred":
            fishID = 3;
            hitpoints = 5;
            break;
        default:
            break;
        }

        if (starter.checkCheatsActive()) {
            hitpoints = 10;
            starter.setBonus(cheatBonus);
        }
        starter.setLifes(hitpoints);

        fred = fredApp.createFred(fishID);
        fredHeight = fred.height;
        fredWidth = fred.width;
        gameScene.addChild(fred);
        fred.play();
    }


    function setupHurtEffect() {
        var texture;
        texture = PIXI.Texture.fromImage('res/images/hurteffect.png');
        hurtEffect = new PIXI.Sprite(texture);
        hurtEffect.width = gameScene.width;
        hurtEffect.height = gameScene.height;
        gameScene.addChild(hurtEffect);
        hurtEffect.visible = false;
    }

    function play() {

        fred.x += fred.vx;
        fred.y += fred.vy;

        checkFredHitsWall();
        checkEating();
        checkPowerup();
        checkHitHook();

        checkShark();

    }


    function gameLoop() {

        if (playing) {

            requestAnimationFrame(gameLoop);

            play();

            renderer.render(stage);

        }
    }

    function setup() {
        var i,
            j;

        setupBackground();
        setupHurtEffect();
        setupFred();
        for (i = 0; i < numberOfWorms; i++) {
            newWorm();
        }

        for (j = 0; j < numberOfHooks; j++) {
            newHook();
            newHook();
        }

        renderer.render(gameScene);
        gameLoop();
    }








    function initControl() {

        var left = keyboard(37),
            up = keyboard(38),
            right = keyboard(39),
            down = keyboard(40);

        left.press = function () {

            turned = false;
            if (invincible) {
                invincibleFred();
            } else {
                changeFred();
            }



            if (fishID === 1) {
                fred.vx = -10;
                fred.vy = 0;
            } else if (fishID === 3) {
                fred.vx = -2;
                fred.vy = 0;
            } else {
                fred.vx = -5;
                fred.vy = 0;
            }

        };
        left.release = function () {
            if (!right.isDown && fred.vy === 0) {
                fred.vx = 0;
            }
        };


        up.press = function () {

            if (fishID === 1) {
                fred.vx = 0;
                fred.vy = -10;
            } else if (fishID === 3) {
                fred.vx = 0;
                fred.vy = -2;
            } else {
                fred.vx = 0;
                fred.vy = -5;
            }

        };
        up.release = function () {
            if (!down.isDown && fred.vx === 0) {
                fred.vy = 0;
            }
        };

        right.press = function () {

            turned = true;
            if (invincible) {
                invincibleFred();
            } else {
                changeFred();
            }

            if (fishID === 1) {
                fred.vx = 10;
                fred.vy = 0;
            } else if (fishID === 3) {
                fred.vx = 2;
                fred.vy = 0;
            } else {
                fred.vx = 5;
                fred.vy = 0;
            }

        };
        right.release = function () {
            if (!left.isDown && fred.vy === 0) {
                fred.vx = 0;
            }
        };


        down.press = function () {
            if (fishID === 1) {
                fred.vx = 0;
                fred.vy = 10;
            } else if (fishID === 3) {
                fred.vx = 0;
                fred.vy = 2;
            } else {
                fred.vx = 0;
                fred.vy = 5;
            }

        };
        down.release = function () {
            if (!up.isDown && fred.vx === 0) {
                fred.vy = 0;
            }
        };
    }


    function keyboard(keyCode) {
        var key = {};
        key.code = keyCode;
        key.isDown = false;
        key.isUp = true;
        key.press = undefined;
        key.release = undefined;
        key.downHandler = function (event) {
            if (event.keyCode === key.code) {
                if (key.isUp && key.press) {
                    key.press();
                    key.isDown = true;
                    key.isUp = false;
                }
                event.preventDefault();
            }
        };
        key.upHandler = function (event) {
            if (event.keyCode === key.code) {
                if (key.isDown && key.release) {
                    key.release();
                    key.isDown = false;
                    key.isUp = true;
                }
                event.preventDefault();
            }
        };

        window.addEventListener("keydown", key.downHandler.bind(key), false);
        window.addEventListener("keyup", key.upHandler.bind(key), false);
        return key;
    }



    function setInvincibleFalse() {
        invincible = false;
    }



    function initPixi() {
        var loader;

        collision = App.FFCollision;
        objects = App.FFObjects;
        starter = App.FFStarter;
        fredApp = App.FFFred;
        powerupApp = App.FFPowerup;

        loader = new PIXI.loaders.Loader();
        stage = new PIXI.Container();

        gameScene = new PIXI.Container();
        stage.addChild(gameScene);

        gameOverScene = new PIXI.Container();
        stage.addChild(gameOverScene);
        gameOverScene.visible = false;
        renderer = new PIXI.autoDetectRenderer(GENERAL_WIDTH, GENERAL_HEIGHT, {
            backgroundColor: 0x1099bb
        }, {
            view: document.getElementById("games-canvas")
        });


        $("#canvas-cover").append(renderer.view);
        loader.add("res/images/underwater.png");
        loader.add("res/images/fred1.png");
        loader.add("res/images/rfred1.png");
        loader.add("res/images/rfastFred1.png");
        loader.add("res/images/rstrongFred1.png");
        loader.add("res/images/fastFred1.png");
        loader.add("res/images/strongFred1.png");
        loader.add("res/images/invFred1.png");
        loader.add("res/images/rinvFred1.png");

        loader.add("res/images/fred2.png");
        loader.add("res/images/rfred2.png");
        loader.add("res/images/rfastFred2.png");
        loader.add("res/images/rstrongFred2.png");
        loader.add("res/images/fastFred2.png");
        loader.add("res/images/strongFred2.png");
        loader.add("res/images/invFred2.png");
        loader.add("res/images/rinvFred2.png");

        loader.add("res/images/worm.png");
        loader.add("res/images/hook.png");

        loader.add("res/images/shark1.png");
        loader.add("res/images/lshark1.png");

        loader.add("res/images/shark2.png");
        loader.add("res/images/lshark2.png");

        loader.add("res/images/hurteffect.png");
        loader.add("res/images/powerup.png");


        loader.load(setup);

        if (starter.checkCheatsActive()) {
            cheatBonus = 10;
        }

    }




    function init() {
        initPixi();
        initControl();
    }



    init();
    setInvincibleFalse();

};