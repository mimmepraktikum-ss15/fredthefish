var App = App || {};
App.FFPowerup = (function () {
    "use strict";
    /* eslint-env browser */
    /*global PIXI */


    function newPowerup(GENERAL_WIDTH, GENERAL_HEIGHT) {
        var powerup,
            texturePowerup = PIXI.Texture.fromImage('res/images/powerup.png');
        powerup = new PIXI.Sprite(texturePowerup);
        powerup.x = Math.random() * GENERAL_WIDTH;
        powerup.y = Math.random() * GENERAL_HEIGHT;

        return powerup;
    }


    function choosePowerupFunction() {

        return Math.floor((Math.random() * 6) + 1);
    }

    return {
        newPowerup: newPowerup,
        choosePowerupFunction: choosePowerupFunction
    };





}());