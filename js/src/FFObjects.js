var App = App || {};
App.FFObjects = (function () {
    "use strict";
    /* eslint-env browser */
    /*global PIXI */


    function newWorm(GENERAL_WIDTH, GENERAL_HEIGHT) {
        var worm,
            textureWorm = PIXI.Texture.fromImage('res/images/worm.png');
        worm = new PIXI.Sprite(textureWorm);
        worm.width /= 2;
        worm.height /= 2;
        worm.x = Math.random() * GENERAL_WIDTH;
        worm.y = Math.random() * GENERAL_HEIGHT;

        return worm;

    }



    function newHook(GENERAL_WIDTH, GENERAL_HEIGHT) {
        var hook,
            textureHook = PIXI.Texture.fromImage('res/images/hook.png');
        hook = new PIXI.Sprite(textureHook);
        hook.width *= 1.5;
        hook.height *= 1.5;
        hook.x = Math.random() * GENERAL_WIDTH;
        hook.y = Math.random() * GENERAL_HEIGHT;

        return hook;
    }


    function newShark(GENERAL_WIDTH, GENERAL_HEIGHT, sharkType) {
        var shark,
            textureShark1,
            textureShark2,
            textures;

        if (sharkType === 1) {
            textureShark1 = PIXI.Texture.fromImage('res/images/shark1.png');
            textureShark2 = PIXI.Texture.fromImage('res/images/shark2.png');
            textures = [textureShark1, textureShark2];
            shark = new PIXI.extras.MovieClip(textures);
            shark.width /= 2;
            shark.height /= 2;
            shark.vx = 2;
            shark.vy = 2;
        } else if (sharkType === 2) {
            textureShark1 = PIXI.Texture.fromImage('res/images/shark1.png');
            textureShark2 = PIXI.Texture.fromImage('res/images/shark2.png');
            textures = [textureShark1, textureShark2];
            shark = new PIXI.extras.MovieClip(textures);
            shark.vx = 3;
            shark.vy = 3;
        }
        shark.animationSpeed = 0.1;
        shark.x = Math.random() * GENERAL_WIDTH;
        shark.y = Math.random() * GENERAL_HEIGHT;

        return shark;

    }


    return {
        newWorm: newWorm,
        newHook: newHook,
        newShark: newShark
    };





}());